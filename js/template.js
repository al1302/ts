(function($){
    $(document).on('ready',function() {
        slider();
        toggleViews();
        mobileMenu();
        mobileFilter();
        pageSteps();
        tabs();
    });

    function slider() {
        $('.similar-slider').slick({
            slidesToShow: 6,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
        $('.personal__slider').slick({
            slidesToShow: 5,
            variableWidth: true,
            lazyLoad: 'ondemand',
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.reviews__slider').slick({
            slidesToShow: 4,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });

        $('.full-slider').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            variableWidth: true,
            dots: false,
            arrows: false,
            infinite: false,
            asNavFor: '.nav-slider',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.nav-slider').slick({
            slidesToShow: 9,
            variableWidth: true,
            dots: false,
            infinite: false,
            arrows: true,
            slidesToScroll: 1,
            asNavFor: '.full-slider',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 5,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 380,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.js-main-slider').slick({
            slidesToShow: 1,
            variableWidth: false,
            dots: true,
            infinite: false,
            arrows: false,
            slidesToScroll: 1,
            focusOnSelect: true,
            autoplay: true,
        });


        $('.js-news-slider').slick({
            slidesToShow: 2,
            variableWidth: false,
            lazyLoad: 'ondemand',
            dots: false,
            arrows: true,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.js-developer-2').slick({
            slidesToShow: 2,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        $('.js-completed-projects').slick({
            slidesToShow: 6,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
        $('.js-partners__block').slick({
            slidesToShow: 5,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                }
            ]
        });
        $('.js-bonus').slick({
            slidesToShow: 4,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
        $('.js-awards').slick({
            slidesToShow: 7,
            dots: false,
            lazyLoad: 'ondemand',
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('.js-programs').slick({
            slidesToShow: 6,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('.js-main-people').slick({
            slidesToShow: 5,
            lazyLoad: 'ondemand',
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.js-m-istor').slick({
            slidesToShow: 3,
            variableWidth: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    }

    function toggleViews() {
        $('.rent__panel--views li').on('click', function(event) {

            var classes = $('.page-content__views').attr('class').split(' ');
            $('.page-content__views').removeClass(classes[2]);
            var target = $(this).attr('data-target');
            $('.page-content__views').addClass(target);
            $('.rent__panel--views li').removeClass('active');
            $(this).addClass('active');

            if (target == 'map-view'){
                var width = $('.main-wrapper').css('width');
                $('.js-map-outer').css('width', width);
                $('.js-map-outer>div').css('width', width);
                map.container.fitToViewport();
            }
            if (target == 'tile'){
                var width = parseInt($('.main-wrapper').css('width')) / 2;
                console.log(width);
                $('.js-map-outer').css('width', width);
                $('.js-map-outer>div').css('width', width);
                map.container.fitToViewport();
            }

        });
    }

    function mobileMenu() {
        $('.mobile-menu-button').on('click', function() {
            $('body').addClass('menu-opened');
        });
        $('.top-menu-mobile-close').on('click', function() {
            $('body').removeClass('menu-opened');
        });
    }

    function mobileFilter() {
        $('.filter-mobils-button').on('click', function() {
            $('body').addClass('filter-opened');
        });
        $('.mobile-filter-close').on('click', function() {
            $('body').removeClass('filter-opened');
        });
    }

    function pageSteps() {
        $('.steps-item__header').on('click', function(){
            var block = $(this).parent().parent().parent();
            $('.steps-item__header').children('.icon-arrows').removeClass('left').addClass('down');
            block.children().children().removeClass('active');
            $(this).parent().addClass('active');
            $(this).children('.icon-arrows').removeClass('down').addClass('left');
        });
    }

    function tabs() {
        $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

        $('.js-tabs').on('click', 'div:not(.f)', function(event) {
            event.stopPropagation();
            var objTabs = $(this).closest('.js-alltabs');
            if (!$(this).closest('.filter-index__mobile-block-title').hasClass('filter-index__mobile-block-title--active')){
                $('.filter-index__mobile-block-title--active').removeClass('filter-index__mobile-block-title--active');
                $(this).closest('.filter-index__mobile-block-title').addClass('filter-index__mobile-block-title--active');
                $('.js-alltabs ul.tabs__caption').find('li:not(.active)').click();
            }
        });
    }

    //--фиксируем шапку
    var header = $('.header__page-panel');
    var hederHeight = header.height(); // вычисляем высоту шапки
    var mobMenu=$('.header__top-panel');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 1 && window.innerWidth<768) {
            header.addClass('menu-header-fixed');
            mobMenu.addClass('menu-mobile-fixed');
            $('header').css({
                'heigth': hederHeight + 'px' // делаем отступ у body, равный высоте шапки
            });
        } else {
            header.removeClass('menu-header-fixed');
            mobMenu.removeClass('menu-mobile-fixed');
            $('header').css({
                'heigth': 100 + '%' // удаляю отступ у body, равный высоте шапки
            })
        }
    });

})(jQuery);

function onlyNums(event) {
    var theEvent = event || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}